# gp-activity #


### Part one ###

* The DNA sequence of the human reference's chromosome 22 is given. This DNA sequence contains genes, which encode functional proteins, the genes coordinates are provided in a given annotations file. 
* The first exercise is to generate a table of gene names sorted by the masses of their encoded proteins.

### Part two ###

* Given a set of mutations for a certain individual, we want to know the difference in mass between the proteins encoded by the mutated genes, and the reference sequence one.
* The second exercise is to generate a plot showing the relationship between the absolute change in mass after mutations, and the original mass of the encoded proteins for all genes.

### provided files ###

```
	data/
	|
	|_ chr22.fa => DNA sequence of the human reference's chromosome 22
	|
	|_ geneannotations.gtf => human reference gene annotations (coordinates of genes in the reference seq)
	|
	|_ mutations.vcf => a list of mutations that a certain individual have

```
### Background ###

* Around 1~2% of the human DNA encodes a functional protein. 
* From the start of a certain coding region in the DNA, each 3 consecutive nucleotides are called a codon. After the start codon, each codon encodes a certain amino acid, these amino acids together forms the encoded protein.
* To get the mass of a protein encoded by a gene, the summation of the masses of the amino acids encoded by the DNA sequence are calculated.


### methodology ####

* A code performing the analysis can be written in any programming language, however python is highly preferred.
* you can use libraries and packages.
* You can ask for help, feedback, and guidance whenever you need it and while you are doing the exercises. It's not an exam.
* Code is expected to be put in a public repository, with contributions from everyone.
* A simple and clean code is most appreciated.
* A simple and clean code is most appreciated (one more time)
* the code is expected to work on any machine. You can use a virtual environment and freeze all the dependencies (used libraries), or provide it as an executable.

#### bonus ####
* there is a small bonus if you generate the graphs using R, ie the python code performs the analysis and exports the results, which are then plotted using an R script. A bash script can control this "pipeline".
* another bonus is there if you download the chr1 sequence and perform the same analysis on it. Keep in mind chr1 is very much bigger than chr22, this point is about how efficient in terms of execution time your code is.

